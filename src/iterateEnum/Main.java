package iterateEnum;

public class Main {
	enum Days {
	   SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
	}
	public static void main(String args[]) {
	      Days days[] = Days.values();
	      System.out.println("Contents of the enum are: ");
	      //Iterating enum using the for loop
	      for(Days day: days) {
	         System.out.println(day);
	      }
	   }
}
