package luckyNine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import luckyNine.Player.Type;

public class Game {
	ArrayList<Player> players = new ArrayList<Player>();
	Deck deck = new Deck();
	
	Player player;
	Player computer;
	
	Scanner scanner = new Scanner(System.in);

	final int MINIMUM_DRAWS = 2;
	final int LUCKY_NINE = 9;	
	final int MAXIMUM_NUM_OF_PLAYERS = 16;
	final int TIE_GAME = 20;

	public Game() {
		int playersAdded = addPlayers();
		for (int i = 1; i <= playersAdded; i++) {
			player = new Player(Type.Player, "Player " + i); 
			players.add(player);
			displayPlayers(player.name);
		}
		computer = new Player(Type.Computer, "Computer");
		players.add(computer);
		

		play();
	}
	
	public int addPlayers() {
		int numberOfPlayers = 0;
		do {
			System.out.println("Enter the number of players joining: ");
			numberOfPlayers = scanner.nextInt();
			if (numberOfPlayers <= 0 || numberOfPlayers > MAXIMUM_NUM_OF_PLAYERS)
				System.out.println("Only 1 to 16 players are allowed!");
		} while (numberOfPlayers <= 0 || numberOfPlayers > MAXIMUM_NUM_OF_PLAYERS);
		return numberOfPlayers;
	}
	
	public void displayPlayers(String...players) {
		for (String player : players) 
			System.out.println(player + " joined.");
	}

	public void play() {
		System.out.println("Let's play a round of lucky nine!");
		System.out.println("The house has shuffled a deck of cards!");

		do {
		drawRound();
		} while(players.get(0).countCards()<MINIMUM_DRAWS);
		
		for (Player player : players)
			if (player != computer) {
				askDrawAgain(player);
				player.printCards(player); 
			}
//		askDrawAgain();
		
		computer.addToHand(deck.draw()); //computer third draw?
		
		pressEnter();
		computer.printCards(computer);
		
		getWinner();
		for (Player player : players)
			player.clearHand();
		playAgain();
		
	}
	
	public void getWinner() {
		for (Player player : players)
			System.out.println(player.name + "'s hand has a value of " + player.getValue());
		System.out.println("The computer's hand has a value of " + computer.getValue());
		
		int winner = getHighestScore();
		
		if (winner == TIE_GAME)
			System.out.println("It's a tie!");
		else if(winner == players.size() - 1)
			System.out.println("Computer wins! Better luck next time");
		else 
			System.out.println(players.get(winner).name + " won!");
	}
	
	public int getHighestScore() {
		List<Integer> scores = new ArrayList<Integer>();
		for (Player player : players)
			scores.add(player.getValue());
//		int computerIndex = scores.size()-1;
//		scores.remove(scores.size()-1);
		System.out.println(scores);
		int highScore = Collections.max(scores);
		int highScoredPlayer = scores.indexOf(highScore);	
		scores.remove(highScoredPlayer);
		if (highScore == Collections.max(scores))
			return TIE_GAME;
		else
			return highScoredPlayer;
	}	
	
	private void playAgain() {
		System.out.println();
		System.out.println("Do you want to play again?(0/1)");
		if(deck.countCards()>=(players.size()*MINIMUM_DRAWS))	{
			int choice = scanner.nextInt();
			
			switch(choice) {
			case 0:
				break;
			case 1:
				askNewDeck();
				play();
				break;
			default:
				System.out.println("That is not a valid input. Try again!");
				playAgain();
				break;
			}
		}
		else	{
			System.out.println("Not enough cards for new game. Shuffling a new deck.");
			deck = new Deck();
			pressEnter();
			play();
		}
	}

	private void askNewDeck() {
		System.out.println();
		System.out.println("Do you want a new deck?(0/1)");
		int choice = scanner.nextInt();
		switch(choice) {
		case 0:
			askShuffle();
			break;
		case 1:
			deck = new Deck();
			break;
		default:
			System.out.println("That is not a valid input. Try again!");
			  break;
		}
	}

	private void askShuffle() {
		System.out.println();
		System.out.println("Do you want to shuffle the deck?(0/1)");
		int choice = scanner.nextInt();
		switch(choice) {
		case 0:
			break;
		case 1:
			deck.shuffleDeck();
			break;
		default:
			System.out.println("That is not a valid input. Try again!");
			  break;
		}
	}

	private void askDrawAgain(Player player) {
		System.out.println();
		System.out.println(player.name + ", do you want to draw again?(0/1)");
		int choice = scanner.nextInt();
		switch(choice) {
		case 0:
			break;
		case 1:
			player.addToHand(deck.draw());
			printDrawnCards(player);
			break;
		default:
			System.out.println("That is not a valid input. Try again!");
			askDrawAgain(player); 
			break;
		};
	}

	public void printDrawnCards(Player player) {
		System.out.println(player.name + " drew a " + player.checkLastCard());
		System.out.println(player.name + " is now holding " + player.countCards() + " card/s");
		System.out.println(player.name + " got a total value of " + player.getValue());
	}

	public void drawRound() {
//		pressEnter();
		
		System.out.println("Cards are now dealt");
		TalkingCards card;

		for (Player player : players) {
			card = deck.draw();
			player.addToHand(card);
			if (player != computer) { //print all players except computer nice
				pressEnter();
				printDrawnCards(player);
				System.out.println();
			} 	
		}
	}
	public void pressEnter() {
		System.out.println();
		System.out.println("Press enter to continue!");
		scanner.nextLine();
	}

	
}
