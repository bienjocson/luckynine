package luckyNine;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;



public class Deck {

	Stack<TalkingCards> deck = new Stack<>();
	
	public Deck() {
		TalkingCards card;
		for(Suit suit : Suit.values()) {
			for(Rank rank : Rank.values()) {
				card=new TalkingCards(suit,rank);
				deck.push(card);
				//System.out.println(card.rank + " of " + card.suit);
				//count++;
			}
		}
		shuffleDeck();
	}
	public void shuffleDeck() {
		Collections.shuffle(deck);
	}
	public boolean isEmpty() {
		return deck.isEmpty();
	}
	public int countCards() {
		return deck.size();
	}
	public TalkingCards draw()	{
		TalkingCards card = deck.pop();
		//card.talk();
		return card;
	}
	public void addCard(TalkingCards card)	{
		deck.push(card);
	}
}
