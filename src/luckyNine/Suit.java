package luckyNine;

public enum Suit {
	    Diamonds,
	    Hearts,
	    Spades,
	    Clubs
}
