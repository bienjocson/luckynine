package luckyNine;

public class TalkingCards {
	final Suit suit;
	final Rank rank;
	public TalkingCards(Suit suit, Rank rank) {
		this.suit=suit;
		this.rank=rank;
	}
	public String talk()	{
		return this.rank + " of " + this.suit + "(" + this.rank.getValue() + ")";
	}
}
