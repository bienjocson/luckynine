package luckyNine;

import java.awt.List;
import java.util.ArrayList;
import java.util.Stack;

public class Player {
	Stack<TalkingCards> hand = new Stack<TalkingCards>();
	Type type;
	String name;
	public Player(Type type, String name) {
		this.type=type;
		this.name = name;
	}
	
	public void addToHand(TalkingCards card) {
		hand.add(card);
	}
	public int countCards()	{
		return hand.size();
	}

	public String checkLastCard() {
		TalkingCards card = hand.peek();
		return card.talk();
	}

	public int getValue() {
		int value = 0;
		for (TalkingCards card : hand) { 		      
			value+=card.rank.getValue();
		}
		return value%10;
	}
	public void printCards(Player player) {
		System.out.println();
		System.out.println(player.name + " drew the following cards");
		for (TalkingCards card : hand) {
			System.out.println(card.talk());
		}
		System.out.println(player.name + " got a total value of " + getValue());
		System.out.println();
	}
	
	public void clearHand() {
		hand.clear();
	}
	public enum Type {
		Player,
		Computer
	}
}
